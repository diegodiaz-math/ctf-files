# README

This repository contains the synthetic code used in the following publication:


1) Diego H. Diaz Martinez, Washington Mio, and Facundo Memóli. The Shape of Data and Probability Measures. Applied and Computational Harmonic Analysis. https://doi.org/10.1016/j.acha.2018.03.003. 2018



## Summary

*.zip file that contains three folders (lines and parabolas, three lines, three planes) with 250 random samples each.